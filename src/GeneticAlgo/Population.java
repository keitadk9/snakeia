package GeneticAlgo;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import ReseauxNeurone.ReseauNeurone;
import Snake.Snake;
import processing.core.PApplet;
import processing.core.PVector;

public class Population implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3320087514876435148L;
	
	private ArrayList<Snake> population;
	private float mutation;
	private int generation;
	private ArrayList<Snake> pool;
	private Snake SnakeCharger;
	private float bestFitnessEver;
	private boolean modePartag�;
	private boolean modeSauvAuto;
	private int tailleInitiale;
	
	public Population(int nombre,int tailleInitiale,float mutationRate,float largeur,float hauteur,boolean modePartag�, boolean modeSauvAuto) {
		generation = 1;
		this.modePartag� = modePartag�;
		this.tailleInitiale = tailleInitiale;
		try {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(new FileInputStream("Ressource/Sauvegarde/meilleurFitness.txt"));
			bestFitnessEver = Float.parseFloat(sc.next());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		this.mutation = mutationRate;
		pool = new ArrayList<Snake>();
		population = new ArrayList<Snake>();
		for(int i = 0 ; i < nombre;i++) {
			population.add(new Snake(new PVector(largeur/2,hauteur/2),tailleInitiale,25f,24,new int[] {16},4,modePartag�));
		}
		SnakeCharger = null;
		this.modeSauvAuto =  modeSauvAuto;
	}
	

	public Population(int nombre,int tailleInitiale,float mutationRate,float largeur,float hauteur,String best,boolean modePartag�,boolean modeSauvAuto) throws FileNotFoundException {
		
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(new FileInputStream("Ressource/Sauvegarde/meilleurFitness.txt"));
		bestFitnessEver = Float.parseFloat(sc.next());
		
		this.modePartag� = modePartag�;
		generation = 1;
		this.mutation = mutationRate;

		this.tailleInitiale = tailleInitiale;
		pool = new ArrayList<Snake>();
		population = new ArrayList<Snake>();
		SnakeCharger = charger(best);

		this.modeSauvAuto =  modeSauvAuto;
		
		
		
		for(int i = 0 ; i < nombre;i++) {
			Snake s = new Snake(SnakeCharger,tailleInitiale,modePartag�);
			s.mutate(mutationRate);
			population.add(s);
		}
	}
	
	
	public boolean  update(float largeur , float hauteur,PVector pomme) {
		
		boolean mang� = false;
		for (Snake snake : population) {
			
				
			
			if(snake.isDead()) {
				continue;
			}
			PVector cible;
			if(!snake.isModePartag�()) {
				cible = snake.getPomme();
			}
			else {
				cible = pomme.copy();
			}
			
			
			double[] inputs = snake.getInformation(cible,largeur,hauteur);
			snake.prendreDecision(inputs);
			snake.update(largeur,hauteur);
			
			
			PVector positionS = snake.getPosition();
			positionS.add(new PVector(Snake.TAILLE_RECT/2,Snake.TAILLE_RECT/2));
	
			
		
			
			float distance = Math.abs(cible.dist(positionS));
			
			if(!snake.isModePartag�() && distance < Snake.TAILLE_RECT) {
				snake.grow();
				snake.setNewPomme();
			}
			else {
				if( distance < Snake.TAILLE_RECT && !mang�) {
					snake.grow();
						mang� = true;
				}
			}
			
			
			
		}
		return mang�;
	}
	
	public void show(PApplet p,float largeur,float hauteur) {
		p.noFill();
		p.stroke(255);
		p.rect(0,0,largeur,hauteur);
		for (Snake snake : population) {
			if(snake.isDead())
				continue;
			
		//	p.fill(255,0,0);
		//	if(!snake.isModePartag�())
			//	p.ellipse(snake.getPomme().x,snake.getPomme().y,16, 16);
			snake.show(p);
		}
	}
	
	public boolean nouvellePopulation() {
		
		float maxF = getBestFitness();
		
		if(modeSauvAuto) {
			for(Snake snake : population) {
				if(snake.getFitness() > bestFitnessEver) {
					try {
						sauvegarde(snake);
						SnakeCharger = snake.clone();
						bestFitnessEver = snake.getFitness();
						setNewBestEver(bestFitnessEver);
							
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if(maxF < 0.001) {
			int nombre = population.size();
			population.clear();
			
			generation = 0;
			if(SnakeCharger == null) {
				for(int i = 0 ; i < nombre;i++) {
					population.add(new Snake(new PVector(1000/2,1000/2),1,25f,60,new int[] {4},4,modePartag�));
				}
			}else {
				for(int i = 0 ; i < nombre;i++) {
					Snake s = new Snake(SnakeCharger);
					s.mutate(mutation);
					population.add(s);
				}
			}
			return false;
		}
		
		
		
		for (int i = 0; i < population.size(); i++) {
			
			float pourcent = PApplet.map(population.get(i).getFitness(),0,maxF,0,1);
			pourcent*= 100;
			for (int j = 0; j < pourcent; j++) {
				pool.add(population.get(i));
			}
		}
		
		int nb = population.size();
		population.clear();
		for (int i = 0; i <  nb; i++) {
			Snake r;
			if(Math.random() < 0.5)
				r = getNChild(pool);
			else 
				r = getChildDuplication(pool);
			
			r.mutate(mutation);
			population.add(r);
		}
		generation++;
		pool.clear();
		return true;
	}
	
	public Snake getNChild(ArrayList<Snake> liste) {
		Random r = new Random();
		
		Snake p1 = liste.get(r.nextInt(liste.size()));
		Snake p2 = liste.get(r.nextInt(liste.size()));
		
		return new Snake(p1,tailleInitiale,new ReseauNeurone(p1.getCerveau(),p2.getCerveau()));

	}
	
	public static Snake getChildDuplication(ArrayList<Snake> liste) {
		Random r = new Random();
		
		Snake p1 = liste.get(r.nextInt(liste.size()));
		
		return p1.clone();
	}
	public int getGeneration() {
		return generation;
	}
	
	public float getBestFitness() {
		float maxF = 0;
		for (Snake snake : population) {
			if(snake.getFitness() > maxF) {
				maxF = snake.getFitness();
			}
		}
		return maxF;
	}
	
	
	
	public boolean End() {
		for (Snake snake : population) {
			if(!snake.isDead())
				return false;
		}
		return true;
	}
	
	public int getCompte�Rebours() {
		
		for (Snake snake : population) {
			if(!snake.isDead())
				return snake.getTimeToLive();
		}
		return 0;
	}
	
	public void sauvegarde() throws FileNotFoundException {
		int max = 0;
		for (int i = 0; i < population.size(); i++) {
			if(population.get(i).getFitness() > population.get(max).getFitness())
				max = i;
		}
		sauvegarde(population.get(max));
		
	}
	
	public void sauvegarde(Snake s) throws FileNotFoundException {
	
		Snake best = s;
		String nom = choisirPr�nom();
		
		System.out.println(nom);
		ObjectOutputStream lien = null;
		try {
			FileOutputStream fichier = new FileOutputStream("Ressource/Sauvegarde/"+best.getFitness()+" "+nom+".ser");
			lien = new ObjectOutputStream(fichier);
			lien.writeObject(best);
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(lien != null) {
					lien.flush();
					lien.close();
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	public float getBestFitnessEver() {
		return bestFitnessEver;
	}
	
	
	
	public Snake charger(String nom) throws FileNotFoundException {
		
		
		ObjectInputStream lien = null;
		try {
			FileInputStream fichier = new FileInputStream("Ressource/Sauvegarde/"+nom+".ser");
			lien = new ObjectInputStream(fichier);
			return (Snake) lien.readObject();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(lien != null) {
					lien.close();
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return null;
	}
	
	private String choisirPr�nom() throws FileNotFoundException {
		
		@SuppressWarnings("resource")
		Scanner file = new Scanner(new FileInputStream("Ressource/Prenom.txt"));
		int pr�nom =(int) ((float)Math.random()*12442);

		for(int i = 0 ; i < pr�nom ; i++) {
			file.nextLine();
		}
		
		return file.nextLine();
	}
	
	private void setNewBestEver(float data) {
	        File file = new File("Ressource/meilleurFitness.txt");
	        FileWriter fr = null;
	        try {
	            fr = new FileWriter(file);
	            fr.write(String.valueOf(data));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }finally{
	            //close resources
	            try {
	                fr.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }

	
	
	
	
	
}
