package Snake;
import ReseauxNeurone.Matrice;
import ReseauxNeurone.ReseauNeurone;

import java.io.Serializable;
import java.util.LinkedList;
import processing.core.PApplet;
import processing.core.PVector;

public class Snake implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3665945084189144040L;
	public static final float TAILLE_RECT = 25;
	private PVector position,deplacement;
	private float vitesse;
	private int taille;
	private int point;
	private LinkedList<PVector> tails;
	private ReseauNeurone cerveau;
	private int nombreInputs,nombreOutput;
	private int[] hiddenLayer;
	private int lifeTime,timeToLive;
	private boolean isDead;
	private static final int TIMETOLIVE = 1500;
	private boolean modePartagé;
	private PVector pomme;
	
	
	public Snake(PVector pst,int t,float v,int nombreInput,int[] hiddenLayer, int nombreOutput,boolean modePartagé) {
		position = pst.copy();
		deplacement = new PVector();
		taille = t;
		vitesse = v;
		lifeTime = 0;
		point = 0;
		isDead = false;
		timeToLive = TIMETOLIVE;
		tails = new LinkedList<PVector>();
		this.nombreInputs = nombreInput;
		
		this.hiddenLayer = hiddenLayer.clone();
		this.nombreOutput = nombreOutput;
		cerveau = new ReseauNeurone(nombreInput,hiddenLayer,nombreOutput, 0.1f);
		this.modePartagé = modePartagé;
		if(!modePartagé)
			pomme = new PVector((float)Math.random()* (1000-275),(float)Math.random()*(700-275)+50);
	}
	
	
	public Snake(Snake parent) {
		this(new PVector(350,350),parent.taille, parent.vitesse,parent.nombreInputs,parent.hiddenLayer,parent.nombreOutput,parent.modePartagé);
		this.cerveau = parent.cerveau.clone();
	}
	
	public Snake(Snake parent,int tailleI,boolean ModePartagé) {
		this(new PVector(350,350),tailleI, parent.vitesse,parent.nombreInputs,parent.hiddenLayer,parent.nombreOutput,ModePartagé);
		this.cerveau = parent.cerveau.clone();
	}
	
	public Snake(Snake parent,int tailleI,ReseauNeurone cerveau) {
		this(new PVector(350,350),tailleI, parent.vitesse,parent.nombreInputs,parent.hiddenLayer,parent.nombreOutput,parent.modePartagé);
		this.cerveau = cerveau.clone();
		
	}
	
	public Snake(Snake p1,Snake p2) {

		this(new PVector(350,350),4, p1.vitesse,p1.nombreInputs,p1.hiddenLayer,p1.nombreOutput,p1.modePartagé);
		this.cerveau = ReseauNeurone.crossOver(p1.cerveau,p2.cerveau);
	}
	
	public Snake(PVector pst,int t,float v,boolean modePartagé) {
		position = pst.copy();
		deplacement = new PVector();
		taille = t;
		vitesse = v;
		tails = new LinkedList<PVector>();
		this.modePartagé = modePartagé;
	}
	
	public void prendreDecision(double[] inputs) {
		prendreDecision(new Matrice(inputs));
	
	}
	public void prendreDecision(Matrice inputs) {
		
		
		double[][] response = cerveau.output(inputs).getTab();
		int indexMax = 0;
		for(int i = 0 ; i < response.length ;++i) {
	//		System.out.println(response[i][0]);
			if(response[i][0] > response[indexMax][0])
				indexMax = i;
		}
	//	System.out.println();
	//	System.out.println(indexMax);
		if(indexMax == 0)
			setDeplacement(new PVector(0,-1));
		else if(indexMax == 1 )
			setDeplacement(new PVector(0,1));
		else if(indexMax == 2 )
			setDeplacement(new PVector(-1,0));
		else if(indexMax == 3)
			setDeplacement(new PVector(1,0));	
		
			
	}
		
	
	
	
	public void setDeplacement(PVector direction) {
		PVector test = deplacement.copy();
		test.setMag(1);
		test.mult(-1);
		if(!test.equals(direction))
			deplacement = direction.copy();
	}
	
	public void update(float largeur ,float hauteur) {

		if(isDead)
			return;
		
		
		timeToLive--;
		lifeTime++;
		
		
		
		if(tails.size() == taille) {
			for (int i = tails.size()-1; i > 0 ; --i) {
				tails.set(i,tails.get(i-1));
			}
				
		}else {
			tails.addFirst(position.copy());	
		}
		if(!tails.isEmpty())
			tails.set(0,position.copy());
		
		deplacement.setMag(vitesse);
		position.add(deplacement);
		if(collision(largeur, hauteur)) {
			isDead = true;
		}
	}
	
	public boolean collision(float largeur,float hauteur) {
		if(position.x+TAILLE_RECT/2+deplacement.x < -25 || position.x+TAILLE_RECT/2+deplacement.x > largeur+25 || position.y+TAILLE_RECT/2+deplacement.y < -25 || position.y+TAILLE_RECT/2+deplacement.y > hauteur+25 )
			return true;
		
		for(PVector p : tails) {
			PVector centreTete = position.copy();
			centreTete.add(TAILLE_RECT/2,TAILLE_RECT/2);
			
			PVector centreP = p.copy();
			centreP.add(TAILLE_RECT/2,TAILLE_RECT/2);
			
			if(Math.abs(centreP.dist(centreTete)) < TAILLE_RECT)
				return true;
			
			
			
			
		}
		return false;
	}
	
	public double[] getInformation(PVector pomme,float largeur,float hauteur) {
		
		double[] inputs = new double[24];
		int indice = 0;
		PVector[] deplacement = new PVector[] {
				new PVector(-1,0),
				new PVector(-1,-1),
				new PVector(0,-1),
				new PVector(1,-1),
				new PVector(1,0),
				new PVector(1,1),
				new PVector(0,1),
				new PVector(-1,1)};
		
		for (PVector pVector : deplacement) {
			int i = (int)pVector.x;
			int j = (int)pVector.y;
			if(i == 0 && j== 0 )
				continue;
			
			//position fictive
			PVector positionF = getPosition();
			//déplacement fictif
			PVector deplacementF = new PVector(i,j);
			//deplacementF.setMag(vitesse);
			

			positionF.add(deplacementF);
			int nombreDeplacement = 1;
			
			float[] perception = new float[3];
			boolean nourritureTrouvé = false;
			boolean murTrouvé = false;
			boolean queuTrouvé = false;
			
			while(isCoordValide(positionF, largeur, hauteur)) {
				
				
				if(!nourritureTrouvé && Math.abs(positionF.dist(pomme))  < TAILLE_RECT) {
					perception[0] = 1;
					nourritureTrouvé = true;
				}

				if(!queuTrouvé && isOnTail(positionF)) {;
					perception[1] = (float) 1/nombreDeplacement;
					queuTrouvé = true;
				}
				
				// on ajoute le deplacement à la position
				positionF.add(deplacementF);
				++nombreDeplacement;
			}
			
			perception[2] =(float) 1/(nombreDeplacement);

			inputs[indice] = perception[0];
			inputs[indice+1] = perception[1];
			inputs[indice+2] = perception[2];

			indice+= 3;;
		}
		
	
		return inputs;
	}
	
	
	public boolean isOnTail(PVector v) {
		
		for(PVector vect : tails) {
			PVector centreTete = vect.copy();
			centreTete.add(TAILLE_RECT/2,TAILLE_RECT/2);
			
			if(Math.abs(centreTete.dist(v)) < TAILLE_RECT)
				return true;
			
		}
		return false;
		
	}
	public boolean isCoordValide(PVector v,float largeur, float hauteur) {
		
		return v.x >= 0 && v.x <= largeur && v.y >= 0 && v.y <= hauteur;
	}
	
	public void show(PApplet p) {
		
	
		float v = 255f;
		float r = PApplet.map(point,0,10,255,0);
		float b = r;
			
		p.fill(r,v,b);
		
		//if(!modePartagé)
		//	p.ellipse(pomme.x, pomme.y,16,16);
		
		p.stroke(0);
		
		for(PVector vect : tails) {
			p.rect(vect.x,vect.y,TAILLE_RECT,TAILLE_RECT);
		}
	
		p.rect(position.x,position.y,TAILLE_RECT,TAILLE_RECT);
		
	}
	
	public void mutate(float rate) {
		cerveau.mutate(rate);
	}
	
	public PVector getPosition() {
		return position.copy();
	}
	public PVector getDeplacement(){
		return deplacement.copy();
	}
	
	public void grow() {
		point++;
		taille++;
	}
	public int getTaille() {
		return taille*lifeTime;
	}
	
	public float getFitness() {
		if(point < 10) {
			return (float) ((float)  lifeTime*lifeTime * Math.pow(2,point));
		}
		
		return (float)  ((float)  lifeTime*lifeTime * Math.pow(2,10));
	}
	
	public boolean isDead() {
		return timeToLive <= 0 || isDead;
	}
	
	public void setNewPomme() {
		pomme = new PVector((float)Math.random()* (1000-275),(float)Math.random()*(700-275)+50);
	}
	public int getLifeTime() {
		return lifeTime;
	}
	public int getTimeToLive() {
		return timeToLive;
	}
	public ReseauNeurone getCerveau() {
		return cerveau.clone();
	}
	
	
	public boolean isModePartagé() {
		return modePartagé;
	}
	public PVector getPomme() {
		return pomme.copy();
	}
	public int getPoint() {
		return point;
	}
	
	public Snake clone() {
		return new Snake(this);
	}
	
	
	
	
	
	
}
