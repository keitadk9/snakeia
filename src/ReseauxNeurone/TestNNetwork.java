package ReseauxNeurone;



import org.junit.Test;

public class TestNNetwork {

	@Test
	public void test() {
		
		ReseauNeurone r = new ReseauNeurone(2,new int[] {30},1,0.01f);
		
		Matrice[][] dataSet = new Matrice[4][2];
		
		dataSet[0] = new Matrice[] {new Matrice(new double[] {0,0}),new Matrice(new double[] {0})};
		dataSet[1] = new Matrice[] {new Matrice(new double[] {0,1}),new Matrice(new double[] {1})};
		dataSet[2] = new Matrice[] {new Matrice(new double[] {1,0}),new Matrice(new double[] {1})};
		dataSet[3] = new Matrice[] {new Matrice(new double[] {1,1}),new Matrice(new double[] {0})};
		
		
		for(int i  = 0 ; i < 500000 ; ++i) {
			int index = (int) ((float)Math.random()*4);
			r.train(dataSet[index][0], dataSet[index][1]);
		}
		
	//	System.out.println(r+"\n");
		
		System.out.println(r.output(new Matrice(new double[] {0,0})));
		System.out.println(r.output(new Matrice(new double[] {0,1})));
		System.out.println(r.output(new Matrice(new double[] {1,0})));
		System.out.println(r.output(new Matrice(new double[] {1,1})));
		
		
		
		
		
	}

}
