package Main;
import java.io.FileNotFoundException;

import GeneticAlgo.Population;
import processing.core.PApplet;
import processing.core.PVector;

public class Appli extends PApplet{
	
	
	Population population;
	
	int NOMBREPOP = 1000;
	boolean situationR�el = true ;
	boolean modePartag� = false;
	boolean modeSauvegardeAuto = true;
	int tailleInitiale =1;
	float mutationRate = 0.01f;
		
	
	String best = "1.71992678E9 Bennet ";
	PVector pomme;
	public static void main(String[] args) {
		PApplet.main(Appli.class);
	}
	
	public void settings() {
		size(1000,700);
	}
	
	public void setup() {
		if(situationR�el)
			population = new Population(NOMBREPOP,tailleInitiale, mutationRate, width, height,modePartag�,modeSauvegardeAuto);
		else {
			try {
				population = new Population(NOMBREPOP,tailleInitiale ,mutationRate, width, height,best,modePartag�,modeSauvegardeAuto);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		pomme = new PVector(random(width-275),random(height-275)+50);
	}
	
	public void draw() {
		//frameRate(15f);
		background(50);
		
		boolean mang� = population.update(width-275, height, pomme.copy());
		
		if(mang�) {
			pomme = new PVector(random(width-275),random(height-275)+50);
		}
		
		if(population.End())
			population.nouvellePopulation();
		
		
		population.show(this,width-275,height);
		
		fill(255);
		textSize(15);
		text("Compte � rebourt: "+population.getCompte�Rebours(),width-275,30);
		text("Taille de la population: "+NOMBREPOP,width-275,55);
		text("Taux de mutation: "+mutationRate*100+"%",width-275,80);
		text("G�n�ration: "+population.getGeneration(),width-275,105);
		text("Best fitness Ever: "+nf(population.getBestFitnessEver(),3,2),width-275,130);
		text("Best fitness: "+nf(population.getBestFitness(),1,2),width-275,155);
		if(!situationR�el)
			text("Cerveau charg�: "+best.split(" ")[1],width-275,180);
		
		fill(255,0,0);
		
		if(modePartag�)
			ellipse(pomme.x,pomme.y,16,16);
		
		
	}
	
	
	public void keyPressed() {
		if(key == ' ' ) {
			System.out.println("Sauvegarder");
			try {
				population.sauvegarde();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else if(key == 'n') {
			population.nouvellePopulation();
		}
	}
	

	
	

	

}
